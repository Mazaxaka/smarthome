﻿using ECM7.Migrator.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThinkingHome.Plugins.Jalusi.Migrations
{
    [Migration(4)]
    public class Migration04_row_removed : Migration
    {
        public override void Apply()
        {
            Database.RemoveColumn("Jalusi_State", "LastUpdate");
        }

        public override void Revert()
        {
           // Database.AddColumn("Jalusi_State", "LastUpdate");
        }
    }
}
