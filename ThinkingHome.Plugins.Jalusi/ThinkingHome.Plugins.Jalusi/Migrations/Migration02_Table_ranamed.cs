﻿using ECM7.Migrator.Framework;

namespace ThinkingHome.Plugins.Jalusi.Migrations
{
    [Migration(2)]
    public class Migration02_Table_ranamed : Migration
    {
        public override void Apply()
        {
            Database.RenameTable("Jalusi", "JalusiState");
        }

        public override void Revert()
        {
            Database.RenameTable("JalusiState", "Jalusi");
        }
    }
}
