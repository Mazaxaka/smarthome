﻿using ECM7.Migrator.Framework;
using System.Data;

namespace ThinkingHome.Plugins.Jalusi.Migrations
{
    [Migration(1)]
    public class Migration01 : Migration
    {
        public override void Apply()
        {
            Database.AddTable("Jalusi",
                new Column("Id", DbType.Guid, ColumnProperty.PrimaryKey, "newid()"),
                new Column("Channel1", DbType.Int32, ColumnProperty.NotNull),
                new Column("Channel2", DbType.Int32, ColumnProperty.NotNull),
                new Column("DisplayName", DbType.String.WithSize(255), ColumnProperty.NotNull),
                new Column("LastUpdate", DbType.DateTime, ColumnProperty.NotNull, false)
            );
            
        }

        public override void Revert()
        {
            Database.RemoveTable("Jalusi");
        }
    }
}
