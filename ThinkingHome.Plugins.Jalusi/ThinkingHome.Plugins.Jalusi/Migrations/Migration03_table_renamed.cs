﻿using ECM7.Migrator.Framework;

namespace ThinkingHome.Plugins.Jalusi.Migrations
{
    [Migration(3)]
    public class Migration03_table_renamed : Migration 
    {
        public override void Apply()
        {
            Database.RenameTable("JalusiState", "Jalusi_State");
        }

        public override void Revert()
        {
            Database.RenameTable("Jalusi_State", "JalusiState");
        }
    }
}
