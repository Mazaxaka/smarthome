﻿using NHibernate.Linq;
using NHibernate.Mapping.ByCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ThinkingHome.Core.Plugins;
using ThinkingHome.NooLite;
using ThinkingHome.Plugins.Jalusi.Model;
using ThinkingHome.Plugins.Listener.Api;
using ThinkingHome.Plugins.Listener.Attributes;
using ThinkingHome.Plugins.Scripts;
using ThinkingHome.Plugins.WebUI.Attributes;

namespace ThinkingHome.Plugins.Jalusi
{
    [Plugin]
    [AppSection("Jalusi", SectionType.Common, "/webapp/jalusi/index.js", "ThinkingHome.Plugins.Jalusi.Resources.index.js")]
    [JavaScriptResource("/webapp/jalusi/index-view.js", "ThinkingHome.Plugins.Jalusi.Resources.index-view.js")]
    [JavaScriptResource("/webapp/jalusi/index-model.js", "ThinkingHome.Plugins.Jalusi.Resources.index-model.js")]

    [HttpResource("/webapp/jalusi/item-template.tpl", "ThinkingHome.Plugins.Jalusi.Resources.item-template.tpl")]
    [HttpResource("/webapp/jalusi/list-template.tpl", "ThinkingHome.Plugins.Jalusi.Resources.list-template.tpl")]
    public class JalusiPlugin : PluginBase
    {
        object              locker = new object();
        object              moLock = new object();
        object              tiLock = new object();
        List<JalusiInfo>    jalusiInfo;
        public override void InitPlugin()
        {
            Logger.Debug("init");
            base.InitPlugin();
        }

        // вызывается после того, как все плагины инициализированы
        public override void StartPlugin()
        {
            Logger.Debug("start");
            base.StartPlugin();
            jalusiInfo = new List<JalusiInfo>();
            using (var session = Context.OpenSession())
            {
                // Загружаем из базы информацию о жалюзи
                var jalusi = session.Query<JalusiState>().ToList();
                foreach (JalusiState jState in jalusi)
                {
                    jalusiInfo.Add(new JalusiInfo(jState));
                }
                Logger.Debug("data drom DB cached");
            }
            
        }

        // вызывается при остановке сервиса
        public override void StopPlugin()
        {
            Logger.Debug("stop");
            base.StopPlugin();
        }

        public override void InitDbModel(ModelMapper mapper)
        {
            mapper.Class<JalusiState>(cfg => cfg.Table("Jalusi_State"));
        }

        [HttpCommand("/api/jalusi/add")]
        public object AddJalusi(HttpRequestParams request)
        {
            var channel1 = request.GetRequiredInt32("channel1");
            var channel2 = request.GetRequiredInt32("channel2");
            var name = request.GetRequiredString("displayName");
            lock (locker)
            {
                //Проверяем, может жалюзи с такими каналами уже зарегистрированы
                JalusiInfo jalusi;
                var jalusiWithChannels = GetJalusi(channel1, channel2);
                var jalusiWithName = GetJalusi(name);
                if (jalusiWithChannels == null)
                {
                    jalusi = jalusiWithName;
                }
                else
                {
                    jalusi = jalusiWithChannels;
                }
                if (jalusi == null)
                {
                    // Регистрируем жалюзи с такими каналами
                    jalusi = new JalusiInfo(Guid.NewGuid(), channel1, channel2, name);
                    // Добавляем жалюзи в наш список
                    jalusiInfo.Add(jalusi);
                    // Добавляем жалюзи в БД
                    using (var session = Context.OpenSession())
                    {
                        session.Save(new JalusiState(jalusi));
                        session.Flush();
                    }
                }
                return jalusi.Id;
            }
        }

        [HttpCommand("/api/jalusi/del")]
        public object DeleteJalusi(HttpRequestParams request)
        {
            var guid = request.GetRequiredGuid("guid");
            lock (locker)
            {
                //Проверяем, есть ли такие жалюзи в базе
                JalusiInfo jalusi = GetJalusi(guid);
                if (jalusi != null)
                {
                    // Удаляем из нашего списка
                    jalusiInfo.Remove(jalusi);
                    // Удаляем из БД
                    using (var session = Context.OpenSession())
                    {
                        session.Delete(new JalusiState(jalusi));
                        session.Flush();
                        return jalusi.Id; // возвращаем ID удаленных жалюзи
                    }
                }
                else
                {
                    // Если жалюзи нет в базе, то возвращаем 0
                    return 0;
                }
            }
        }

        [HttpCommand("/api/jalusi/get")]
        public object GetJalusi(HttpRequestParams request)
        {
            var jalusi = GetJalusi(request.GetRequiredInt32("channel1"), request.GetRequiredInt32("channel2"));
            return jalusi;
        }

        [HttpCommand("/api/jalusi/get-list")]
        public object GetJalusiList(HttpRequestParams request)
        {
            int index  = request.GetRequiredInt32("index");
            int count  = request.GetRequiredInt32("count");
            int maxind = jalusiInfo.Count();
            List<JalusiInfo> jalusiList;
            if (maxind > index)
            {
                if (maxind > index + count)
                {
                    jalusiList = jalusiInfo.GetRange(index, count);
                }
                else
                {
                    jalusiList = jalusiInfo.GetRange(index, maxind - index);
                }
            }
            else
            {
                jalusiList = new List<JalusiInfo>();
            }
            return jalusiList;
        }

        [ScriptCommand("nooliteSetJalusiState")]
        public void SetJalusiState(string name, int stateId, int secondsToStop)
        {
            var jalusiId = GetJalusi(name).Id;
            var NooLite = new ThinkingHome.Plugins.NooLite.NooLitePlugin();
            var jalusi = ChangeLastUpdateTime(jalusiId);
            Logger.Debug(jalusi.LastUpdate.ToString());
            Logger.Debug(jalusi.Channel1.ToString() + " " + jalusi.Channel2.ToString() + " " + secondsToStop.ToString());
            var waitTime = new TimeSpan(0, 0, secondsToStop);
            switch (stateId)
            {
                //опустить
                case 1:
                    lock (moLock)
                    {
                        NooLite.SendCommand((int)PC11XXCommand.SetLevel, jalusi.Channel1, 0);
                        NooLite.SendCommand((int)PC11XXCommand.SetLevel, jalusi.Channel2, 100);
                        Logger.Debug("The curtain falls ");
                    }
                    if (!CheckJalusi(jalusi, waitTime)) { return; }
                    lock (moLock)
                    {
                        NooLite.SendCommand((int)PC11XXCommand.SetLevel, jalusi.Channel2, 0);
                        NooLite.SendCommand((int)PC11XXCommand.SetLevel, jalusi.Channel1, 0);
                        Logger.Debug("The curtain omitted ");
                    }
                    break;
                //поднять
                case 2:
                    lock (moLock)
                    {
                        NooLite.SendCommand((int)PC11XXCommand.SetLevel, jalusi.Channel1, 100);
                        NooLite.SendCommand((int)PC11XXCommand.SetLevel, jalusi.Channel2, 100);
                        Logger.Debug("The curtain rises ");
                    }
                    if (!CheckJalusi(jalusi, waitTime)) { return; }
                    lock (moLock)
                    {
                        NooLite.SendCommand((int)PC11XXCommand.SetLevel, jalusi.Channel2, 0);
                        NooLite.SendCommand((int)PC11XXCommand.SetLevel, jalusi.Channel1, 0);
                        Logger.Debug("The curtain rose ");
                    }
                    break;
                //остановить
                default:
                    lock (moLock)
                    {
                        NooLite.SendCommand((int)PC11XXCommand.SetLevel, jalusi.Channel2, 0);
                        NooLite.SendCommand((int)PC11XXCommand.SetLevel, jalusi.Channel1, 0);
                        Logger.Debug("The curtain motor stopped!");

                    }
                    break;

            }
            
        }

        private bool CheckJalusi(JalusiInfo jalusi, TimeSpan timeSpan)
        {
            DateTime myTime = jalusi.LastUpdate;
            while (DateTime.Now < myTime + timeSpan)
            {
                Thread.Sleep(1000);
                var newjal = GetJalusi(jalusi.Id);
                if (newjal.LastUpdate > myTime)
                {
                    Logger.Debug("Task started at " + myTime.ToString() + " stopped. Reason: Another action started!");
                    return false;
                }
            }
            return true;
        }

        private JalusiInfo GetJalusi(Guid jalusiId)
        {
                return jalusiInfo.FirstOrDefault<JalusiInfo>((j) => j.Id == jalusiId);
        }

        private JalusiInfo GetJalusi(string jalusiName)
        {
            return jalusiInfo.FirstOrDefault<JalusiInfo>((j) => j.Name == jalusiName);
        }

        private JalusiInfo GetJalusi(int channel1, int channel2)
        {
            return jalusiInfo.FirstOrDefault<JalusiInfo>((j) => j.Channel1 == channel1 && j.Channel2 == channel2);
        }

        private JalusiInfo ChangeLastUpdateTime(Guid jalusiId)
        {
            int ind;
            lock (locker)
            {
                ind = jalusiInfo.FindIndex(j => j.Id == jalusiId);
                jalusiInfo[ind].LastUpdate = DateTime.Now;
            }
            return jalusiInfo[ind];
        }
    }
}
