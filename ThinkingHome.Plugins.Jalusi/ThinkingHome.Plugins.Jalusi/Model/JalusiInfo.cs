﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThinkingHome.Plugins.Jalusi.Model
{
    public class JalusiInfo
    {
        public Guid Id { get; set; }

        public int Channel1 { get; set; }

        public int Channel2 { get; set; }

        public string Name { get; set; }

        public DateTime LastUpdate { get; set; }

        public JalusiInfo()
        {
            Id         = Guid.Empty;
            Channel1   = 0;
            Channel2   = 0;
            Name       = "NonInitialized";
            LastUpdate = DateTime.MinValue;
        }

        public JalusiInfo(Guid id, int channel1, int channel2, string name)
        {
            Id         = id;
            Channel1   = channel1;
            Channel2   = channel2;
            Name       = name;
            LastUpdate = DateTime.Now;
        }
        public JalusiInfo(JalusiState value)
        {
            Id = value.Id;
            Channel1 = value.Channel1;
            Channel2 = value.Channel2;
            Name = value.DisplayName;
            LastUpdate = DateTime.Now;
        }
    }
}
