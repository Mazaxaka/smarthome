﻿using System;

namespace ThinkingHome.Plugins.Jalusi.Model
{
    public class JalusiState
    {
        public virtual Guid Id { get; set; }

        public virtual int Channel1 { get; set; }

        public virtual int Channel2 { get; set; }

        public virtual string DisplayName { get; set; }

        public JalusiState()
        {
            Id          = Guid.Empty;
            Channel1    = 0;
            Channel2    = 0;
            DisplayName = "NonInitialized";
        }

        public JalusiState(Guid ID, int Ch1, int Ch2, string Name)
        {
            Id          = ID;
            Channel1    = Ch1;
            Channel2    = Ch2;
            DisplayName = Name;
        }
         public JalusiState(JalusiInfo value)
        {
            Id          = value.Id;
            Channel1    = value.Channel1;
            Channel2    = value.Channel2;
            DisplayName = value.Name;
        }
    }
}
