﻿define(
   ['app', 'marionette', 'backbone', 'underscore',
       'webapp/jalusi/index-view',
       'webapp/jalusi/index-model'],
function (application, marionette, backbone, _, views) {

    var api = {
        addJalusi: function () {

            var name = this.ui.name.val();
            var channel1 = this.ui.channel1.val();
            var channel2 = this.ui.channel2.val();

            if (name) {
                application
                    .request('cmd:jalusi:add', name, channel1, channel2)
                    .done(api.list);
            }
        },
        delJalusi: function (view) {

            var id = view.model.get('Id');

                application
                    .request('cmd:jalusi:del', id)
                    .done(api.list);
            
        },
        details: function (view) {

            var id = view.model.get('id');
            application.navigate('webapp/jalusi/details', id);
        },
        list: function () {

            var rq = application.request('query:jalusi:get-list', 0, 10);

            $.when(rq).done(function (collection) {

                var view = new views.SensorList({
                    collection: collection
                });

                view.on('childview:show:sensor:details', api.details);
                view.on('add:jalusi', api.addJalusi);
                view.on('childview:del:jalusi', api.delJalusi);
                application.setContentView(view);
            });


        }
    };

    var module = {
        start: api.list
    };
    return module;
});