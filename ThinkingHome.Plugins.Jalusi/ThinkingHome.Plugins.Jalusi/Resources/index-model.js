﻿define(
	['app', 'marionette', 'backbone', 'underscore'],
	function (application, marionette, backbone, _) {

	    var api = {
	        loadJalusis: function (index, count) {

	            var defer = $.Deferred();

	            $.getJSON('/api/jalusi/get-list', {index: index, count: count})
					.done(function (items) {

					    var collection = new backbone.Collection(items);
					    defer.resolve(collection);
					})
					.fail(function () {

					    defer.resolve(undefined);
					});

	            return defer.promise();
	        },

	        addJalusi: function(name, channel1, channel2){
	            var defer = $.getJSON('/api/jalusi/add', { channel1: channel1, channel2: channel2, displayName: name });
	            return defer;
	        },
	        delJalusi: function (guid) {
	            var defer = $.getJSON('/api/jalusi/del', { guid: guid});
	            return defer;
	        }
	    };

	    // requests
	    application.reqres.setHandler('query:jalusi:get-list', api.loadJalusis);
	    application.reqres.setHandler('cmd:jalusi:add', api.addJalusi);
	    application.reqres.setHandler('cmd:jalusi:del', api.delJalusi);

	    return api;
	});