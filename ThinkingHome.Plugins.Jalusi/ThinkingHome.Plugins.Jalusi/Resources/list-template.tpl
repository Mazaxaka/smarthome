﻿<h1>
  jalusi
</h1>
<div class="row">
  <div class="col-md-12">
    <form class="form-inline mc-form-add-sensor" role="form">
      <div class="form-group">
        <label for="tb-display-name">Display name</label>
        <input id="tb-display-name" class="form-control" type="text" />
      </div>
      <div class="form-group">
        <label for="select-channel1">Channel1</label>
        <select id="select-channel1" class="form-control">
          <% for (var i = 0; i
          < 64; i++) {%>
          <option>
            <%= i %>
          </option>
          <% } %>
        </select>
      </div>
      <div class="form-group">
        <label for="select-channel2">Channel2</label>
        <select id="select-channel2" class="form-control">
          <% for (var i = 0; i
          < 64; i++) {%>
          <option>
            <%= i %>
          </option>
          <% } %>
        </select>
      </div>
      <input type="button" class="btn btn-primary js-add-jalusi" value="Add" />
    </form>
  </div>

  <div class="col-md-12">
    <table class="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>channel 1</th>
          <th>channel 2</th>
          <th></th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>

</div>