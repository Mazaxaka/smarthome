﻿define(
	['app', 'marionette', 'backbone', 'underscore',
	 'text!webapp/jalusi/list-template.tpl',	// шаблон для списка объектов
	 'text!webapp/jalusi/item-template.tpl'	// шаблон для элемента списка
	],
	function (application, marionette, backbone, _, tmplList, tmplListItem) {

	    var sensorView = marionette.ItemView.extend({
	        template: _.template(tmplListItem),
	        tagName: 'tr',
	        triggers: {
	            'click .js-del-jalusi': 'del:jalusi'
	        }
	    });

	    var sensorListView = marionette.CompositeView.extend({
	        template: _.template(tmplList),
	        childView: sensorView,
	        childViewContainer: 'tbody',
	        ui: {
	            name: '#tb-display-name',
	            channel1: '#select-channel1',
	            channel2: '#select-channel2'
	        },
	        triggers: {
	            'click .js-add-jalusi': 'add:jalusi'
                
	        }
	    });

	    return {
	        SensorList: sensorListView
	    };
	});